from django.template import Library

register = Library()

@register.filter(name='fieldtype')
def field_type(boundfield):
    """Returns the underlying `Field` type of this `BoundField` as a string."""
    return boundfield.field.__class__.__name__

@register.filter(name='withcss')
def with_css(fld, classes):
    return fld.as_widget(attrs={'class': classes})
