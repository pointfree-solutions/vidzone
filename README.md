
Requirements
============

VidZone are looking to extend their existing web application, a video
gallery. Presently, visitors to the VidZone video gallery are able to:

 * Create a new account
 * Log in with their account
 * Create a channel and upload videos to share
 * Watch videos uploaded by other users

Since videos can be quite large, the VidZone designers have suggested
that, to improve the user experience, Visitors be allowed to add videos 
from third-party file hosting sites Dropbox or Google Drive. Both these 
services provide deep integration with most Operating Systems, making 
uploading files as easy as copying them to an OS folder.

Your mission, should you choose to accept it, is to accquire the source
code for VidZone's web app, and enhance it so users can add videos from
either Dropbox or Google Drive (or both, for extra KICKASS points).

To succeed, you'll need the following:

 1. A good grasp of the Python programming language. 
    If you haven't already done so, download it here: https://www.python.org/downloads/

 2. A good grasp of pip, the most popular Python package manager.
    More information here: https://pip.pypa.io/en/stable/

 3. Some experience building web applications with the Django framework.
    If you're new to Django (or just need a refresher), check out the
    excellent intro: https://docs.djangoproject.com/en/1.11/intro/
    Full documentation here: https://docs.djangoproject.com/en/1.11/

 4. Some experience using git, a Distributed Version Control System.
    You'll need git to "clone" the VidZone source code, as well as to
    "push" your changes back, once you're done.
    More information here: https://git-scm.com/

 5. Some experience with front-end development tools and technologies,
    and knowledge of HTML, CSS and Javascript.

 6. A comfortable place to work, without interruptions, for several hours.
    If you're unable to focus at home, we suggest a nearby library, a 
    friend's place or a co-working space.

 7. A black belt in Goog-fu :-)

To complete the mission, you _must_ perform the following tasks:

 1. Accquire and set up the VidZone application. In order to complete
    this step, you'll have to:
    
    * Clone the repository provided
    * Install all application requirements (see the `requirements.txt` file)
    * Load the sample data fixtures provided for `accounts` and `shows`
    
    Once set up, take screenshots of the home and video detail pages, and 
    save them in the "screenshots" folder - you'll submit them along with 
    your modifications.

 2. Create a new branch named `{firstname}_{lastname}` all lowercase. 
    For example, if your name was "Wade Watts", you'd create a branch named
    "wade_watts". This is _important_ -- we'll check this branch for your
    work. If it doesn't exist, neither does your work :-(

 3. Extend the app as described in the mission statement above. Remember
    to commit your work often; it'll help us follow your thought-process
    while developing. You won't be penalized for not commiting your work
    often, but if you're unable to complete the mission, there won't be
    any record of how much work you've done.

 4. Test the integration using the sample Dropbox and Google Drive accounts
    provided. Take screenshots of the integration in action, and add them to
    the "screenshots" folder.
    To be clear, to consider this step completed, you should be able to
    connect to either of the sample accounts provided, add one of the
    videos there to the app, and play it.

 5. Commit all your changes, and push your new branch to the VidZone repo.

Rules of Engagement:

 1. Third-party libraries are permitted (within reason), so long as we can
    install them using pip. Remember to document all your dependencies.

 2. All work submitted _must_ be done by you. If you're successful, we'll
    probably want to discuss your solution with you, so please don't just
    paste code you copy from Stackoverflow or someone's blog.

 3. If you've any questions, please reply to this email, and we'll get back
    to you. Be sure to ask all your questions today though; we won't be
    taking any questions during the challenge. Also, you should be aware
    that we may share answers to your questions with all other participants,
    if we deem it neccessary. Fairplay, and all that.

Good luck.
