from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from .forms import UserChangeForm, UserCreationForm
from .models import User


class UserAdmin(auth_admin.UserAdmin):
    fieldsets = (
        (None, {
            'fields': ('phone', 'password',)
        }),
        (_('Personal info'), {
            'fields': ('full_name', 'email',)
        }),
        (_('Permissions'), {
            'fields': ('is_active', 'is_superuser', 'groups', 'user_permissions',)
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined',)
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'password1', 'password2',)
        }),
    )
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('phone', 'full_name', 'email', 'is_staff',)
    list_filter = ('is_superuser', 'is_active',)
    search_fields = ('phone', 'full_name',)
    ordering = ('phone',)


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
