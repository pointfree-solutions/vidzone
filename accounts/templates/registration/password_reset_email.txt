Hello {{ user.get_short_name }},

We received a request to reset your password. Click on 
the link below, or copy it and paste it into your browser, 
to reset your password:

{{ protocol }}://{{ domain }}{% url 'accounts:confirm_reset_password' uidb64=uid token=token %}

If you did not make this request, you can safely delete 
this email. Your password will not be changed.

Kind regards,
The WStreams Team