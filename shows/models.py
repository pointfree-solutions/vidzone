from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import os
import random
import time
import uuid


class FeaturedQuerySet(models.QuerySet):
    """Custom ``QuerySet`` to return a random instance of the base model."""

    def featured(self):
        """Return a random featured instance."""
        queryset = self.all()
        count = queryset.aggregate(count=models.Count('pk'))['count']
        if count:
            index = random.randint(0, count - 1)
            return queryset[index]
        return queryset.none()


class Channel(models.Model):
    """A collection of videos shared by a user."""

    owner = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='channel')
    name = models.CharField(_('Channel name'), max_length=150, db_index=True)
    slug = models.SlugField(max_length=150, editable=False)
    description = models.TextField(_('Description'), blank=True)
    created = models.DateTimeField(default=timezone.now, db_index=True, editable=False)

    objects = FeaturedQuerySet.as_manager()

    class Meta:
        verbose_name = _('Channel')
        verbose_name_plural = _('Channels')
        ordering = ('name',)

    def __str__(self):
        return self.name


class License(models.Model):
    """A set of terms under which a show may be consumed."""

    name = models.CharField(_('Name'), max_length=150)
    text = models.TextField(_('Text'), blank=True)
    url = models.URLField(_('URL'), max_length=500)

    class Meta:
        verbose_name = _('Content license')
        verbose_name_plural = _('Content licenses')
        ordering = ('name',)

    def __str__(self):
        return self.name


def set_media_path(instance, filename):
    """Returns a path to the supplied show's media."""
    filename = '%s%s' % (instance.stream_name, os.path.splitext(filename)[1])
    return os.path.join('shows', instance.channel.slug, filename)

class Show(models.Model):
    """A wholesome video that visitors can enjoy."""

    # From https://en.wikipedia.org/wiki/Motion_picture_content_rating_system#Nigeria
    CR_G, CR_PG, CR_12, CR_15, CR_18 = ('G', 'PG', '12', '15', '18',)
    RATINGS = (
        (CR_G, _('Suitable for viewing by persons of all ages')),
        (CR_PG, _('Parental Guidance advised')),
        (CR_12, _('Not suitable for persons under the age of 12')),
        (CR_15, _('Not suitable for persons under the age of 15')),
        (CR_18, _('Not suitable for persons under the age of 18')),
    )

    channel = models.ForeignKey('Channel', related_name='shows')
    stream_name = models.UUIDField(default=uuid.uuid4, db_index=True, editable=False)
    title = models.CharField(_('Title'), max_length=250)
    description = models.TextField(_('Description'), blank=True)
    rating = models.CharField(_('Content Rating'), max_length=2, choices=RATINGS, default=CR_G, db_index=True)
    age_restricted = models.BooleanField(_('Enforce content rating'), default=False,
        help_text=_("Respect this show's rating, preventing underage viewers from seeing it, if neccessary."))
    content_license = models.ForeignKey('License', verbose_name=_('Content License'), 
        related_name='shows', blank=True, null=True)
    poster = models.ImageField(_('Poster'), upload_to=set_media_path, null=True, blank=True,
        help_text=_("Upload a high-resolution image to be used as this show's poster."))
    video = models.FileField(_('Video'), upload_to=set_media_path, help_text=_('Upload a video file.'))
    created = models.DateTimeField(db_index=True, null=True, editable=False, default=timezone.now)

    objects = FeaturedQuerySet.as_manager()

    class Meta:
        verbose_name = _('Show')
        verbose_name_plural = _('Shows')
        ordering = ('-created',)

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('shows:watch', (), {'channel': self.channel.slug, 'show_name': self.stream_name})

